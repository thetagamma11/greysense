# GraySense

A (PF/OPN)Sense Monitoring and Analytics Setup based on Graylog, that should be deployed reproducable and with minimal effort and knowledge

inspired/based by the work of 
* PFSense-Graylog https://github.com/devopstales/pfsense-graylog
* Geolocation via Pipelines https://blog.reconinfosec.com/geolocation-in-graylog/
* IRQ10's Extractors for OPNSense: https://github.com/IRQ10/Graylog-OPNsense_Extractors

**Thanks!**

This is at an early stage, comments & commits are highly appreciated!
  
## Installation
### Prerequisites
 - a working docker environment (have a look here: https://docs.docker.com/install/)
 - (optional but very helpful) [portainer](https://www.portainer.io/), removes the pain of docker 
 - get the MaxMind Light City Database `GeoLite2-City.mmdb` [here](https://dev.maxmind.com/geoip/geoip2/geolite2/)
 - get the CSV for Port to Service Conversion from [devoptales](https://github.com/devopstales/pfsense-graylog/blob/master/service-names-port-numbers/service-names-port-numbers.csv)

### Steps
- clone repository: ```git clone https://gitlab.com/thetagamma11/greysense.git```
- start up the environment
  ```bash
  docker-compose up -f graysense-dockercompose.yml
  ```
  or upload the file in "stacks" of `portainer`
  
  After a while you should be able to login to graylog by pointing your browser to http://yourhost:9009, login: admin/admin

  >#### What happens?
  >Now there will be 4 containers configured: Elasticsearch, MongoDB, Graylog and cerebro. The latter is just an tool to ease Elastic Administration and can be safely omitted.
  I tweaked the [original graylog compose](http://docs.graylog.org/en/stable/pages/installation/docker.html) file with for restart behavior and more persistance

- Now copy the `GeoLite2-City.mmdb` (don't forget to unzip!) and the `service-names-port-numbers.csv` to your persistant graylog config folder (on linux it's something like `/var/lib/docker/volumes/*graylog_config/_data`, if you use portainer, have a look at `Volumes` to find the right place )
- Goto `System > Indices` and create an index called **OPNSense** (sorry PF-Users, it's too cumbersome to maintain 2 contentpacks, perhaps someone else takes care of it) 
   ![Index](/pictures/graysense_index.png)
  >#### What happens?
  >basically speaking: we create some space in our Elasticsearch where we can dump our logs to. Have a look at the index "opnsense_0" with cerebro!
- now upload and install the contentpack "graysense-contentpack.json" by heading to `System > Content Packs` in your graylog
  >#### What happens?
  > * the content pack adds *Extractors* that parse your logfiles and puts the contents in different fields, where you later can search for. The extractors are from [IRQ10](https://github.com/IRQ10/Graylog-OPNsense_Extractors)
  > * the result from the extraction is put into a stream where messages are filtered with the tag "opnsense" and are now stored in the index
  > * in a last step a pipeline is created, where some more magic happens like the geo-tagging, which is based on lookup-Tables to the MaxMind DB

- check if your OPNSense stream is using the index set `OPNSense`
- now go to your (OPN/PF)Sense and configure external logging. 
  * PFSense: go to the Remote Logging Options section and specify in Remote log servers the ip address graylog is running on and the port 4514. 
  * OPNSense: Configure the `Remote Syslog Server Server` in `System > Settings > Logging`

- go back to your graylog and look in `Inputs` whether messages are coming in. do the same in the pipeline and the lookup tables.
- Now you should be able to open the dashboard `OPNSense` and see something like this: ![Dashboard Graysense](pictures/Dashboard_Graysense.png)

By building a nice view, you could also easily search through your logfiles
![](pictures/graylog_view_graysense.png)

## Final remarks
* Although I love Grafana as used by devoptales, I experienced a lot of glitches and bad habits in combination with graylog. For the ease of setup, I rebuild everything in an graylog dashboard - no need to switch tools for analysis and monitoring
* as proposed by graylog, I did not use the build-in geo-ip lookup plugin
* the GROK-Patterns from devoptales did not work very well with IPv6, that's why I've chosen IRQ10's Extractors 


  

